FROM maven:latest AS build
WORKDIR /
COPY src /src
COPY pom.xml /
RUN mvn  clean package

FROM openjdk:alpine
COPY --from=build /target/capstone-0.0.1-SNAPSHOT.war /capstone-0.0.1-SNAPSHOT.war
CMD java -cp /capstone-0.0.1-SNAPSHOT.war com.capstone.App
