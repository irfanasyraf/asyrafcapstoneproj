critical="`grep "CRITICAL" scanfile.log  | wc -l `"
h="`grep "HIGH" scanfile.log  | wc -l `"
m="`grep "MEDIUM" scanfile.log  | wc -l `"

if [ $critical -ge 20 ]
  then
        echo "There are $critical Critical severities so imagescan test has Failed"
        exit 1

elif [ $h -ge 50 ]
  then
        echo "There are $h High serverities so imagescan test has Failed"
        exit 1

elif [ $m -ge 130 ]
  then
        echo "There are $m High serverities so imagescan test has Failed"
        exit 1

else
        echo "There are only $critical Critical, $h High, $m Medium severities so imagescan can be passed"
fi
