(cut -d "," -f4 /home/ubuntu/capstonejavaproj/target/site/jacoco/jacoco.csv | tail -n +2) > missed
(cut -d "," -f5 /home/ubuntu/capstonejavaproj/target/site/jacoco/jacoco.csv | tail -n +2) > covered

awk '{s+=$1} END {print s}' missed > totalmissed
awk '{s+=$1} END {print s}' covered > totalcover
missed=`cat totalmissed`
covered=`cat totalcover`
total=100
echo "Missed=" $missed
echo "Covered="$covered
sum=`expr $missed + $covered`
perc=`expr $missed / $sum`
perc1=`expr $perc \* $total`
test=`echo "scale=2; 100- ($missed / $sum) * 100" | bc`
var1=`awk -v v="$test" 'BEGIN{printf "%d", v}'`
echo "CodeCoverage=" $var1 "%"
if [ $var1 -ge 15 ]
     then
     echo "passed"
     exit 0
     else
     echo "failed"
     exit 1
fi
